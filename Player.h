#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string>

using namespace std;

class Player {
public:
	Wizard wizard;
	int effect;
	bool equipped;
	vector<Spell> activeSpells;


	Player() {

	}

	/* 
	This function is called by Round.
	Calls spells function and decreases its duration.
	If duration is 0 -> remove it from the activeSpellList.
	*/
	void castSpells() {

	}


	Wizard operator=(string str) {
		Wizard temp(this->wizard);

		if (str.compare(" ") == 0) {
			return temp;
		}

		return temp;
	}

	void operator=(int amount) {
		switch (effect) {
		case 0:
			cout << "Damage!" << endl;
			wizard.changeHP(-amount);
			break;
		case 1:
			cout << "Heal!" << endl;
			wizard.changeHP(+amount);
			break;
		case 2:
			cout << "Equip!" << endl;
			this->equipped = true;
		}
	}
	
	void operator=(Wand wand) { 
		this->wizard.setEquipped(wand.wandState);
	}


	Player *operator-(Player *player) {
		player->effect = 0;
		return player;
	}

	Player *operator+(Player *player) {
		player->effect = 1;
		return player;
	}

	Player *operator%(Player *player) {
		player->effect = 2;
		return player;
	}

	void doTurn() 
	{
		this->wizard.showWizardSpells();
		string chosenSpell;
		cin	>> chosenSpell;

	}
};