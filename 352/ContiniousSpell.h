#include <functional>

extern class Player;

using namespace std;

enum Type { FOR, AFTER };

class ContinuousSpell {
public:
	Type type;
	int duration;
	int roundCasted;
	int *currentRound;
	std::function<void()> action;
	Player *attacker;
	Player *defender;
	
	ContinuousSpell() {

	}
	ContinuousSpell(int duration, std::function<void()> action) {
		this->duration = duration;
		this->action = action;
	}

	void castSpell() {
		(*currentRound)++;
		switch (type) {
		case FOR:
			if (duration >= *currentRound - roundCasted) {
				action();
			}
			break;
		case AFTER:
			if (duration == *currentRound - roundCasted) {
				action();
			}
			break;
		}
	}
};