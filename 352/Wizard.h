#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string>

using namespace std;

class Wizard {
private:
	string		name;
	string		house;
	int			maxHp;
	int			hp;
	bool		equipped;

	vector<Spell> spells;

public:
	Wizard() {
	}
	Wizard(const Wizard &copy) {
		name = copy.name;
		house = copy.house;
		hp = copy.hp;
	}
	Wizard(string name, string house, int hp) {
		this->name = name;
		this->house = house;
		this->hp = hp;
		this->maxHp = hp;
	}


	//Getters, Setters:
	void addSpell(Spell spell) {
		spells.push_back(spell);
	}
	vector<Spell> getSpells() {
		return spells;
	}
	int getHP()
	{
		return this->hp;
	}
	string getName() {
		return this->name;
	}
	string getHouse()
	{
		return this->house;
	}

	bool getEquipped() 
	{
		return this->equipped;
	}
	void setEquipped( bool equipped)
	{
		this->equipped = equipped;
	}

	void changeHP(int amount) {
		this->hp += amount;
		if (hp > maxHp)
			hp = maxHp;
	}
	void setHP(int hp) {
		this->hp = hp;
	}
	void setName(string name) {
		this->name = name;
	}
	void setHouse(string house) {
		this->house = house;
	}

	Spell findSpell(string spell) 
	{
		vector <Spell> ::iterator it;
		for (it = spells.begin(); it != spells.end(); ++it)
			if ((*it).name.compare(spell) == 0) 
			{
				return *it;
			}

		Spell emptySpell; emptySpell.name = "$$EMPTY";
		return emptySpell;
	}

	void showWizardSpells()
	{
		vector <Spell> ::iterator it;
		cout << "---------------------------" << endl;

		for (it = spells.begin(); it != spells.end(); ++it)
			cout << it->name << endl;

		cout << "---------------------------" << endl;
	}

	int operator,(Wizard) {
		return 1;
	}
	int operator[](int) {
		return 1;
	}
	int operator[](Spell spell) 
	{
		spells.push_back(spell);
		spells.insert(spells.end(), spell.tempSpells.begin(), spell.tempSpells.end());
		spell.tempSpells.clear();

		return 1;
	}
};