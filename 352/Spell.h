#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string>

using namespace std;


class Spell {
public:
	string					name;
	std::function<void()>	action;
	vector<Spell>			tempSpells;

	Spell() {

	}
	Spell(const Spell &cop) {
		this->name = cop.name;
		this->action = cop.action;
	}
	Spell(string name, std::function<void()> action) {
		this->name = name;
		this->action = action;
		if (this->action == NULL)
			cout << "Constructor mesage: NULL ACTION\n"; 
	}
	Spell(const Spell * cop) {
		this->name = cop->name;
		this->action = cop->action;
		this->tempSpells = cop->tempSpells;
	}

	int operator,(Spell) {
		return 1;
	}
	int operator[](int) {
		return 1;
	}

	Spell operator+(Spell other) {
		tempSpells.push_back(other);
		return Spell(this);
	}	
};