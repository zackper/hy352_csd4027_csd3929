#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string>

using namespace std;

//extern class Player;

class Round {
public:
	vector<Player *> players;
	bool turn;
	int counter;
	bool gameFinished;

	Round() {
		players.push_back(new Player());
		players.push_back(new Player());
		turn = true;
		gameFinished = false;
		counter = 0;
	}

	void nextTurn() {
		turn = !turn;
	}
	Player *getAttacker() {
		return turn ? players[0] : players[1];
	}
	Player *getDefender() {
		return turn ? players[1] : players[0];
	}
		

	void showState()
	{
		for (unsigned int i = 0; i < 2; i++)
		{

			Wizard wizardPlayer = this->players[i]->wizard;
			cout << "###########################" << endl;
			cout << "Name: " << wizardPlayer.getName() << endl;
			cout << "HP: " << wizardPlayer.getHP() << endl;
			(this->players[i]->wizard.getEquipped()) ? cout << "Wand equipped" << endl : cout << "Wand not equipped" << endl;
			cout << "###########################";
			for (unsigned int j = 4; j > i; j = j - 1) cout << endl;

		}
	}

	bool  isGameFinished() 
	{
		if (this->players[0]->wizard.getHP() > 0 && this->players[1]->wizard.getHP() > 0)
			return 0;
		else 
		{
			this->gameFinished = true;
			return 1;
		}
	}

	void getWinner()
	{
		if (this->gameFinished)
		{
			if (this->players[0]->wizard.getHP() <= 0 && this->players[1]->wizard.getHP() <= 0)
			{
				cout << "Tie!" << endl;
				return;
			}
			if (this->players[0]->wizard.getHP() > 0)
				cout << this->players[0]->wizard.getName() << "(Player1) is the winner!" << endl;
			else
				cout << this->players[1]->wizard.getName() << "(Player2) is the winner!" << endl;
		}
	}

};
