#include <functional>

class Action {
public:
	int duration;
	std::function<void()> action;

	Action(std::function<void()> action) {
		this->duration = 1;
		this->action = action;
	}
	Action(int duration, std::function<void()> action) {
		this->duration = duration;
		this->action = action;
	}
};