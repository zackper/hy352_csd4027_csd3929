#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string>
#include <iterator>
#include <tuple>

using namespace std;

class Player {
public:
	Wizard wizard;
	string opponentHouse;
	int effect;
	int rounds;
	vector<ContinuousSpell *> continuousSpells;

	ContinuousSpell * createContinuous() {
		ContinuousSpell *spell = new ContinuousSpell();
		continuousSpells.push_back(spell);

		return spell;
	}

	Player() {
		rounds = 1;
	}

	void doTurn()
	{
		this->wizard.showWizardSpells();
		string chosenSpell;
		cin >> chosenSpell;

		Spell spell = wizard.findSpell(chosenSpell);
		if (spell.name.compare("$$EMPTY") == 0)
		{
			cout << "Haven't learnt that spell yet!\n";
			doTurn();
			return;
		}

		castSpell(this->wizard.findSpell(chosenSpell));
	}

	void castSpell(Spell spell) {
		spell.action();
	}

	void castPreviousSpells() {
		for (unsigned int i = 0; i < continuousSpells.size(); i++) {
			continuousSpells[i]->castSpell();
		}
	}

	Wizard operator=(bool b) {
		if (b) {
			return wizard;
		}

		return wizard;
	}

	void operator=(int amount) {
		int amountFromAttacker = 0;
		int amountFromDefender = 0;

		switch (effect) {
		case 0:
			if (wizard.getHouse().compare("Gryffindor") == 0) {
				if (opponentHouse.compare("Slytherin") == 0)
					amountFromDefender = amount * 0.3;
				else
					amountFromDefender = amount * 0.2;
			}
			else if (wizard.getHouse().compare("Hufflepuff") == 0)
				amountFromDefender = amount * 0.07;
			
			if (opponentHouse.compare("Slytherin") == 0) {
				if (wizard.getHouse().compare("Gryffindor") == 0) {
					amountFromAttacker = amount * 0.2;
				}
				else
					amountFromAttacker = amount * 0.15;
			}
			else if (opponentHouse.compare("Hufflepuff") == 0) {
				amountFromAttacker = amount * 0.07;
			}
			else if (opponentHouse.compare("Ravenclaw") == 0) {
				if (rounds % 2 != 0) {
					cout << "MORE DAMAGE" << endl;
					amountFromAttacker = amount * 0.07;
				}
			}

			//cout << "Damage! " << endl;
			wizard.changeHP(- amount - amountFromAttacker + amountFromDefender);
			break;
		case 1:
			if (wizard.getHouse().compare("Ravenclaw") == 0) {
				if (rounds % 2 == 0) {
					amountFromDefender = amount * 0.05;
				}
			}

			//cout << "Heal!" << endl;
			wizard.changeHP(amount + amountFromDefender);
			break;
		case 2:
			//cout << "Equip!" << endl;
			this->wizard.setEquipped(true);
		}
	}
	
	void operator=(Wand wand) { 
		this->wizard.setEquipped(wand.wandState);
	}
	
	Player *operator-(Player *player) {
		player->effect = 0;
		return player;
	}

	Player *operator+(Player *player) {
		player->effect = 1;
		return player;
	}

	Player *operator%(Player *player) {
		player->effect = 2;
		return player;
	}
};