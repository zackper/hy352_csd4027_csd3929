#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <functional>
#include <list>
#include <map>

#include "ContiniousSpell.h"
#include "Spell.h"
#include "Wizard.h"
#include "Wand.h"
#include "Player.h"
#include "Round.h"

using namespace std;


list<Wizard *>	wizards;
list<Spell *>	spells;


//First the active spells:

//Then the spell done is this round. Then put that spell
//in the previous spell list.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//For creation:

#define CREATE	;

#define WIZARD	*createWizard() = Wizard() =
#define WIZARDS Wizard()

#define SPELL	*createSpell() = Spell() =
#define SPELLS	Spell()

#define NAME	0 ? "null"
#define HOUSE	0 ? "null"
#define HP		0 ? 0
#define ACTION	0 ? (std::function<void()>)[](){}
#define START	[&](){ ContinuousSpell* spell;
#define END		;}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//For spell logic:
#define AND(...)			andOperation({__VA_ARGS__})
#define OR(...)				orOperation({__VA_ARGS__})
#define NOT(...)			notOperation(VA_ARGS)

#define IF					;if(
#define ELSE				;}else{
#define ELSE_IF				;}else if(
#define DO					){

#define FOR					;spell = (roundController.getAttacker())->createContinuous();\
							spell->type = FOR;\
							spell->currentRound = &currentRound; spell->roundCasted = currentRound;\
							spell->attacker = roundController.getAttacker(); spell->defender = roundController.getDefender();\
							spell->duration = 
#define ROUNDS				; spell->action = [&](
#define DO					){

#define AFTER				;spell = (roundController.getAttacker())->createContinuous();\
							spell->type = AFTER;\
							spell->currentRound = &currentRound; spell->roundCasted = currentRound;\
							spell->attacker = roundController.getAttacker(); spell->defender = roundController.getDefender();\
							spell->duration = 


#define DAMAGE				;(*(Player()-
#define HEAL				;(*(Player()+
#define EQUIP				;(*(Player()%

#define ATTACKER			roundController.getAttacker()))=
#define DEFENDER			roundController.getDefender()))=

#define GET_NAME(name)		;((( name  true).getName()		
#define GET_HOUSE(house)	;((( house true).getHouse()
#define GET_HP(hp)			;((( hp  true).getHP()		
#define HAS_WAND(wizard)    ;((( wizard true).getEquipped()

#define BEGIN_GAME			using namespace std;\
							int main(){ \
								;Wand a = { true };\
								Wand _ = { false };\
								int currentRound = 0;
#define END_GAME			;getchar();}
#define DUEL				Duel();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#define MR					(*findWizard(
#define MRS					(*findWizard(
#define LEARN				))
#define SPELL_NAME(name)	+findSpell((#name))

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Spell operator+(Spell spell) {

	return spell;
}

Wand operator-(Wand wand) 
{
	return wand;
}

Wand operator--(Wand wand)
{
	return wand;
}

//EQUIP DEFENDER ---o

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Wizard*		accessWizard(int index) {
	list <Wizard *> ::iterator it = wizards.begin();
	advance(it, index);

	return *it;
}
Spell*		accessSpell(int index) {
	list <Spell *> ::iterator it = spells.begin();
	advance(it, index);

	return *it;
}

Wizard		*createWizard() {
	Wizard *wizard = new Wizard;
	wizards.push_back(wizard);
	return wizard;
}
Spell		*createSpell() {
	Spell *spell = new Spell;
	spells.push_back(spell);
	return spell;
}

void showWizards()
{
	list <Wizard *> ::iterator it;
	for (it = wizards.begin(); it != wizards.end(); ++it) {
		cout << (*it)->getName() << endl;
		
	}

	
}
void showSpells()
{
	list <Spell *> ::iterator it;
	for (it = spells.begin(); it != spells.end(); ++it)
		cout << " " << (*it)->name << endl;

	
	cout << endl;
}


int andOperation(std::initializer_list<int> exp)
{
	std::initializer_list<int>::iterator it;  // same as: const int* it
	int temp = 1;

	for (it = exp.begin(); it != exp.end(); ++it)
	{
		temp = temp && *it;
		cout << *it << endl;
	}

	return temp;
}

int notOperation(int exp)
{
	return !exp;
}

int orOperation(std::initializer_list<int> exp)
{
	return notOperation(andOperation(exp));
}

Wizard* findWizard(string name) {
	list <Wizard *> ::iterator it;
	for (it = wizards.begin(); it != wizards.end(); ++it) {
		if ((*it)->getName().compare(name) == 0)
			return *it;
	}

	return NULL;
}
Spell	findSpell(string name) {
	list <Spell *> ::iterator it;
	for (it = spells.begin(); it != spells.end(); ++it) {
		if ((*it)->name.compare(name) == 0) {
			return *it;
		}
	}

	return NULL;
}

Round roundController;

void initDuel()
{
	cout << "------------------------------------HARRY POTTER THE GAME------------------------------------\n" << endl;

	for (unsigned int i = 0; i < 2; i++)
	{
		cout << "Player" << i + 1 << " select wizard:" << endl << "---------------------------" << endl;

		showWizards();

		cout << "---------------------------" << endl;

		string selectedWizard;
		cin >> selectedWizard;

		roundController.players[i]->wizard = *findWizard(selectedWizard);

		cout << endl;
	}

	roundController.players[0]->opponentHouse = roundController.players[1]->wizard.getHouse();
	roundController.players[1]->opponentHouse = roundController.players[0]->wizard.getHouse();
}

void Duel()
{
	initDuel();
	while (!roundController.gameFinished)
	{

		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Round" << roundController.counter+1 << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

		for (unsigned int i = 0; i < 2; i++)
		{
			Player* currPlayer = roundController.players[i];
			currPlayer->castPreviousSpells();

			roundController.players[i]->rounds++;
			if (!currPlayer->wizard.getEquipped())
				cout << currPlayer->wizard.getName() << "(Player" <<
				i + 1 << ") has not a wand so he can't cast a spell." << endl;
			else
			{
				cout << currPlayer->wizard.getName() << "(Player" << i + 1 <<
					") select spell:" << endl;
				roundController.players[i]->doTurn();
			}
			cout << endl;
			roundController.showState();
			//cout << endl << endl;
			if (roundController.isGameFinished()) 
			{
				roundController.getWinner();
				break;  
			}

			roundController.nextTurn();
		}

		roundController.counter++;
	}
}

