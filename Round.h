#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string>

using namespace std;

class Round {
public:
	bool gameFinished;
	vector<Player *> players;
	bool turn;

	Round() {
		players.push_back(new Player());
		players.push_back(new Player());
		turn = false;
	}

	void nextTurn() {
		turn = !turn;
	}
	Player *getAttacker() {
		return turn ? players[0] : players[1];
	}
	Player *getDefender() {
		return turn ? players[1] : players[0];
	}

	/*bool doRound() {
		//Player1 input

		//players[0].castSpells();
		nextTurn();
		//Player2 input
		nextTurn();

		//Do round effects:


		//if nobody dead:
		return true;
		//if somebody died;
		return false;
	}*/

	void showState() 
	{
		for (unsigned int i = 0; i < 2; i++) 
		{
			
			Wizard wizardPlayer = this->players[i]->wizard;
			cout << "###########################" << endl;
			cout <<	"Name: "<< wizardPlayer.getName() << endl;
			cout << "HP: " << wizardPlayer.getHP() << endl;
			(this->players[i]->equipped) ? cout << "Wand equipped" << endl : cout << "Wand not equipped";
			cout << "###########################";
			for (unsigned int j = 3; j > i; j = j - 1) cout << endl;

		}
	}


};
